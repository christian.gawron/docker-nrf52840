FROM debian:stretch
LABEL maintainer="Christian Gawron <gawron.christian@fh-swf.de>"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    binutils-arm-none-eabi gcc-arm-none-eabi gdb-arm-none-eabi \
    libnewlib-arm-none-eabi libstdc++-arm-none-eabi-newlib \
    automake g++ libtool lsb-release make \
    clang-format \
    wget unzip git
RUN git clone --recursive https://github.com/makerdiary/nrf52840-mdk-usb-dongle.git

WORKDIR /usr/local
RUN wget 'https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2018q2/gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2'
RUN tar xjf gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2

WORKDIR /nrf52840-mdk-usb-dongle/deps/openthread
RUN ./bootstrap
RUN make -f examples/Makefile-nrf52840 clean
RUN make -f examples/Makefile-nrf52840 BORDER_AGENT=1 BORDER_ROUTER=1 COAP=1 COMMISSIONER=1 DNS_CLIENT=1 JOINER=1 LINK_RAW=1 MAC_FILTER=1 MTD_NETDIAG=1 SERVICE=1 UDP_FORWARD=1 ECDSA=1 SNTP_CLIENT=1 COAPS=1 USB=1

WORKDIR /nrf52840-mdk-usb-dongle/nrf_sdks
RUN wget 'https://www.nordicsemi.com/-/media/Software-and-other-downloads/SDKs/nRF5-SDK-for-Thread/nRF5-SDK-for-Thread-and-Zigbee/nRF5SDKforThreadandZigbeev300d310e71.zip'
RUN unzip -d nRF5SDKforThreadandZigbeev300d310e71 nRF5SDKforThreadandZigbeev300d310e71.zip

# WORKDIR /nrf52840-mdk-usb-dongle/examples/openthread/cli/ftd/armgcc
# RUN make

WORKDIR /